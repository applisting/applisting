# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140528232915) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "created_by"
    t.string   "state"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "auctions", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.decimal  "minimum_price_usd"
    t.datetime "begins_at"
    t.datetime "ends_at"
    t.boolean  "sold"
    t.integer  "mobile_app_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "winning_bid_id"
    t.datetime "emailed_winner_at"
    t.datetime "emailed_seller_at"
  end

  add_index "auctions", ["mobile_app_id"], name: "index_auctions_on_mobile_app_id", using: :btree

  create_table "comments", force: true do |t|
    t.string   "title",            limit: 50, default: ""
    t.text     "comment"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.string   "role",                        default: "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["commentable_id"], name: "index_comments_on_commentable_id", using: :btree
  add_index "comments", ["commentable_type"], name: "index_comments_on_commentable_type", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "mobile_app_bids", force: true do |t|
    t.integer  "mobile_app_id"
    t.integer  "user_id"
    t.integer  "bid_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mobile_app_views", force: true do |t|
    t.integer  "mobile_app_id"
    t.string   "request_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mobile_apps", force: true do |t|
    t.string   "label"
    t.string   "app_id"
    t.string   "website_url"
    t.string   "title"
    t.string   "verification_token"
    t.datetime "verified_at"
    t.string   "monetization"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
    t.string   "seller"
    t.string   "category"
    t.datetime "updated"
    t.string   "version"
    t.string   "size"
    t.string   "rating"
    t.string   "compatibility"
    t.string   "developer_website"
    t.string   "developer_apps"
    t.string   "privacy_policy"
    t.string   "app_support"
    t.string   "state"
    t.integer  "delayed_job_id"
    t.text     "tagline"
    t.decimal  "minimum_purchase_price", precision: 12, scale: 2, default: 0.0, null: false
  end

  add_index "mobile_apps", ["user_id"], name: "index_mobile_apps_on_user_id", using: :btree

  create_table "offers", force: true do |t|
    t.decimal  "amount"
    t.boolean  "accepted"
    t.boolean  "canceled"
    t.datetime "paid_at"
    t.integer  "buyer_id"
    t.integer  "seller_id"
    t.integer  "auction_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offers", ["auction_id"], name: "index_offers_on_auction_id", using: :btree
  add_index "offers", ["buyer_id"], name: "index_offers_on_buyer_id", using: :btree
  add_index "offers", ["seller_id"], name: "index_offers_on_seller_id", using: :btree

  create_table "payments", force: true do |t|
    t.integer  "amount"
    t.integer  "auction_id"
    t.integer  "user_id"
    t.string   "credit_uri"
    t.string   "debit_uri"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "screenshots", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "screenshot_file_name"
    t.string   "screenshot_content_type"
    t.integer  "screenshot_file_size"
    t.datetime "screenshot_updated_at"
    t.integer  "mobile_app_id"
    t.string   "category"
  end

  create_table "screenshoturls", force: true do |t|
    t.integer  "mobile_app_id"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", force: true do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                    default: "",   null: false
    t.string   "encrypted_password",       default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "user_name"
    t.string   "create_by"
    t.boolean  "comment_notification_off", default: true
    t.text     "street_address"
    t.string   "zipcode"
    t.string   "customer_uri"
    t.string   "bank_account_uri"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
