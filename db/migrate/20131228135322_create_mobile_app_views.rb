class CreateMobileAppViews < ActiveRecord::Migration
  def change
    create_table :mobile_app_views do |t|
      t.integer :mobile_app_id
      t.string :request_ip
      t.timestamps
    end
  end
end
