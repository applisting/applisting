class AddAttachmentScreenshotToScreenshots < ActiveRecord::Migration
  def self.up
    change_table :screenshots do |t|
      t.attachment :screenshot
    end
  end

  def self.down
    drop_attached_file :screenshots, :screenshot
  end
end
