class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :amount
      t.integer :auction_id
      t.integer :user_id
      t.string :credit_uri
      t.string :debit_uri

      t.timestamps
    end
  end
end
