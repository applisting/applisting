class CreateMobileApps < ActiveRecord::Migration
  def change
    create_table :mobile_apps do |t|
      t.string :label
      t.string :app_store_url
      t.string :website_url
      t.string :name
      t.string :verification_token
      t.datetime :verified_at
      t.string :monetization
      t.references :user, index: true

      t.timestamps
    end
  end
end
