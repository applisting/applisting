class ModifyMinimumPurchasePrice < ActiveRecord::Migration
  def change
    remove_column :mobile_apps, :minimum_purchase_price
    add_column :mobile_apps, :minimum_purchase_price, :decimal, :precision => 12, :scale => 2, :null => false, :default => '0.00'
  end
end