class AddDelayedJobId < ActiveRecord::Migration
  def up
    add_column :mobile_apps, :delayed_job_id, :integer
  end
  
  def down
    remove_column :mobile_apps, :delayed_job_id
  end
end