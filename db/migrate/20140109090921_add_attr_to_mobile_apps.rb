class AddAttrToMobileApps < ActiveRecord::Migration
  def change
    add_column :mobile_apps, :tagline, :text
  end
end