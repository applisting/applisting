class CreateMobileAppBids < ActiveRecord::Migration
  def change
    create_table :mobile_app_bids do |t|
      t.integer :mobile_app_id
      t.integer :user_id
      t.integer :bid_value

      t.timestamps
    end
  end
end
