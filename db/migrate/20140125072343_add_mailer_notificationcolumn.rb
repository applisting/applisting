class AddMailerNotificationcolumn < ActiveRecord::Migration
  def up
    remove_column :users, :comment_notification_enabled
    add_column :users, :comment_notification_off, :boolean, :default => true
  end
  
  def down
    add_column :users, :comment_notification_enabled, :boolean
    remove_column :users, :comment_notification_off
  end
end