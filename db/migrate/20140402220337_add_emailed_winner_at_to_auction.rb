class AddEmailedWinnerAtToAuction < ActiveRecord::Migration
  def change
    add_column :auctions, :emailed_winner_at, :datetime
  end
end
