class AddStateToMobileApp < ActiveRecord::Migration
  def change
    add_column :mobile_apps, :state, :string
  end
end
