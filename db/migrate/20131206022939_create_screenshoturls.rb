class CreateScreenshoturls < ActiveRecord::Migration
  def change
    create_table :screenshoturls do |t|
      t.integer :mobile_app_id
      t.string :url
      t.timestamps
    end
  end
end
