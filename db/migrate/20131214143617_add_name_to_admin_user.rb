class AddNameToAdminUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :name, :string
    add_column :admin_users, :created_by, :string
    add_column :admin_users, :state, :string
  end
end
