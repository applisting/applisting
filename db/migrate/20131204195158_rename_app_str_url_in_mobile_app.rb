class RenameAppStrUrlInMobileApp < ActiveRecord::Migration
  def change
    rename_column :mobile_apps, :app_store_url, :app_id
    rename_column :mobile_apps, :name, :title
    
    add_column :mobile_apps, :description, :text
    add_column :mobile_apps, :seller, :string
    add_column :mobile_apps, :category, :string
    add_column :mobile_apps, :updated, :datetime
    add_column :mobile_apps, :version, :string
    add_column :mobile_apps, :size, :string
    add_column :mobile_apps, :rating, :string
    add_column :mobile_apps, :compatibility, :string
    add_column :mobile_apps, :developer_website, :string    
    add_column :mobile_apps, :developer_apps, :string
    add_column :mobile_apps, :privacy_policy, :string
    add_column :mobile_apps, :app_support, :string
    add_column :mobile_apps, :minimum_purchase_price, :string
  end
end