class AddCategoryToScreenshot < ActiveRecord::Migration
  def change
    add_column :screenshots, :category, :string
  end
end
