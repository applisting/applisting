class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.decimal :amount
      t.boolean :accepted
      t.boolean :canceled
      t.datetime :paid_at
      t.references :buyer, index: true
      t.references :seller, index: true
      t.references :auction, index: true

      t.timestamps
    end
  end
end
