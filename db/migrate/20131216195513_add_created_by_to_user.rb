class AddCreatedByToUser < ActiveRecord::Migration
  def change
    add_column :users, :create_by, :string
    add_column :users, :comment_notification_enabled, :boolean
  end
end
