class CreateAuctions < ActiveRecord::Migration
  def change
    create_table :auctions do |t|
      t.string :title
      t.text :description
      t.decimal :minimum_price_usd
      t.datetime :begins_at
      t.datetime :ends_at
      t.boolean :sold
      t.references :mobile_app, index: true

      t.timestamps
    end
  end
end
