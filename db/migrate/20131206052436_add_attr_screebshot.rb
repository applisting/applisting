class AddAttrScreebshot < ActiveRecord::Migration
  def change
    add_column :screenshots, :mobile_app_id, :integer
    remove_column :screenshots, :label
  end
end