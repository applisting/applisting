class AddEmailedSellerToAuctions < ActiveRecord::Migration
  def change
    add_column :auctions, :emailed_seller_at, :datetime
  end
end
