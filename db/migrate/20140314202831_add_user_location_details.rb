class AddUserLocationDetails < ActiveRecord::Migration
  def up
    add_column :users, :street_address, :text
    add_column :users, :zipcode, :string
  end

  def down
    remove_column :users, :street_address
    remove_column :users, :zipcode
  end
end
