class AddWinningBidIdToAuction < ActiveRecord::Migration
  def change
    add_column :auctions, :winning_bid_id, :integer
  end
end
