require 'scheduled_job'
class VerifyApp < Struct.new(:mobile_app)

  include Delayed::ScheduledJob

  run_every 10.minutes

  def before(job)
    @job = job
  end

  def display_name
    "AppAuctionJob"
  end

  def perform
    puts "job running"

    if mobile_app.android?
      app_data = Nokogiri::HTML(open(mobile_app.app_id))
      app_description = app_data.css("div.app-orig-desc,div.id-app-orig-desc").first.content.strip
    elsif mobile_app.ios?
      app_data = ITunesSearchAPI.lookup(:id => mobile_app.ios_app_id)
      app_description = app_data["description"]
    end

    if app_description.include?(mobile_app.verification_token.to_s)
      mobile_app.publish!
      @job.destroy
    else
      raise "Could not find verification token in #{mobile_app.label} store link"
    end

    #FakeWeb.register_uri(:get, mobile_app.app_id, :body => "Hello World! #{mobile_app.verification_token.to_s}")
    #app_description = Net::HTTP.get(URI.parse(mobile_app.app_id)).split(' ').collect! {|n| n.to_s}
    #if app_description.include?(mobile_app.verification_token.to_s)
      #mobile_app.publish!
      #@job.destroy
    #else
      #raise 'An error has occured'
    #end
  end

  def max_attempts
    return 3
  end
end
