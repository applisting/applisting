desc 'Finalize Auctions and Payment'
task :finalize_auctions => :environment do
  #Get all auctions recently completed but not emailed in the past 24 hours
  completed_auctions = Auction.where("ends_at <= ?", Time.now).where("sold IS NULL OR sold = ?", false).where("emailed_winner_at IS NULL OR emailed_winner_at <= ?", 1.day.ago)
  puts "Found #{completed_auctions.size} completed auctions to notify"
  completed_auctions.each do |auction|
    if auction.mobile_app.nil?
      puts "No mobile app for auction #{auction.id}; skipping notifications"
      next
    end

    if (auction.sold.nil? || !auction.sold?) && auction.mobile_app.mobile_app_bid.any?
      puts "Sending auction completed notifications to buyer and seller for auction #{auction.id}"
      #Auction has not been finalized yet
      winning_bid = auction.mobile_app.mobile_app_bid.order("bid_value DESC").first
      winning_bid_value = winning_bid.bid_value.to_i
      auction.winning_bid_id = winning_bid.id
      winning_user = winning_bid.user
      AuctionsMailer.finalize_auction(auction.mobile_app, auction, winning_user).deliver
      AuctionsMailer.notify_seller(auction.mobile_app, auction).deliver
      auction.emailed_winner_at = Time.now
      auction.emailed_seller_at = Time.now
      auction.save!(:validate => false)
    elsif auction.emailed_seller_at.nil?
      puts "Sending notification to seller for auction #{auction.id}"
      AuctionsMailer.notify_seller(auction.mobile_app, auction).deliver
      auction.emailed_seller_at = Time.now
      auction.save!(:validate => false)
    else
      puts "No notifications to send for auction #{auction.id}"
    end
  end
end
