AppAuction::Application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users

  get '/search' => 'home#search', as: 'search'
  get '/users/payment' => 'users#payment', as: :payment
  post '/users/payment' => 'users#payment_post', as: :payment_post 
  resources :users, :only => [:show, :edit, :update, :payment]
  resources :mobile_apps, :except => [:show]
  resources :mobile_app_steps
  resources :home, :only => [:show, :index]
  get 'contacts/new', :as => "contact"
  post 'contacts/create'
  
  root :to => 'home#index'
  post '/screenshot_upload' => 'mobile_apps#screenshot_upload'
  get '/screenshot_upload_status' =>  'mobile_apps#screenshot_upload_status'
  delete '/screenshot_delete' =>  'mobile_apps#screenshot_delete'
  
  resources :auctions, :only => [:show] do
    get "/bids", :to => "auctions#bids", :as => :bids
  end
  post 'auctions/place_bid'
  post 'auctions/create_comments'
  post 'mobile_apps/create_comments'

  get "/payments", to: "payments#index", as: :auction_payments
  get "/payments/:id", to: "payments#show", as: :auction_payment
  get "/payments/:id/pay", to: "payments#pay", as: :auction_payment_pay
  post "/payments/:id/pay", to: "payments#pay_post", as: :auction_payment_pay_post
end
