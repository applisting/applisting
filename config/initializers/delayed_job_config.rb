Delayed::Worker.destroy_failed_jobs = false
Delayed::Worker.sleep_delay = 10
Delayed::Worker.max_attempts = 100
Delayed::Worker.max_run_time = 1.minutes
Delayed::Worker.read_ahead = 10
Delayed::Worker.default_queue_name = 'default'