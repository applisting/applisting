module MobileAppsHelper

  def link_to_remove_fields(name, f, remove_tag=false)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this,#{remove_tag})", :class => "button tiny alert")
  end

  def link_to_add_fields(name, f, association, path)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(path + association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\");setUpload()")
    # NOTE: modify this method if "link_to_add_fields" is being used in other places
  end

  def step_class(step)
    if current_step == step
      "active-step"
    elsif MobileApp::STEPS.index(current_step) > MobileApp::STEPS.index(step)
      "completed-step"
    end
  end

  def current_step
    params[:step].presence || MobileApp::STEPS.first
  end

end
