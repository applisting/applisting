module UsersHelper
  def auction_label_class(status)
    case status
    when "active"
      "success"
    when "ended"
      ""
    when "not yet started"
      ""
    else
      "alert"
    end
  end
end
