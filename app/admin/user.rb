ActiveAdmin.register User do

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  scope :registered, :default => true do |users|
    users.confirmed
  end

  scope :pending_confirmation do |users|
    users.not_confirmed
  end
  
  controller do
    
    before_filter :set_current_admin_user
    
    def set_current_admin_user
      User.current = current_admin_user
    end
    
    def permitted_params
      params.permit user: [ :name, :email, :password, :password_confirmation, :user_name, :comment_notification_off ]
    end
  end
  
  actions :all, :except => [:new]
  
  filter :email, :filters =>[:equals]
  filter :name, :filters =>[:equals]
  filter :created_at, :label =>"Created Date"
  filter :confirmed_at, :label =>"Confirmed Date"
  
  index download_links: [:csv] do
    selectable_column
    column :email
    column :name
    column "Created Date", :created_at
    column "Confirmed Date", :confirmed_at
    default_actions
  end
  
  show do |user|
    panel "User Details", :class => "comments" do
      div :class=> "user_image" do
        div do
          image_tag avatar_url(user)
        end
        div do
          span do
            "Turn off comment email notifications from App Listing Site :"
          end
          span do
            check_box "user", "comment_notification_off", {:checked => user.comment_notification_off, :disabled => true}
          end
        end
      end
      br do
      end
      div :class=> "show_user_details" do
        div do
          span do
            "Email :"
          end
          span do
            user.email
          end
        end
        div do
          span do
            "Name :"
          end
          span do
            user.name
          end
        end
        div do
          span do
            "No. of Apps Uploaded :"
          end
          span do
            user.mobile_apps.size unless user.mobile_apps.nil?
          end
        end
        div do
          span do
            "No. of Apps Sold :"
          end
          span do
            "TODO"
          end
        end
        div do
          span do
            "No. of Apps Bought :"
          end
          span do
            "TODO"
          end
        end
        div do
          span do
            "Date Created :"
          end
          span do
            user.created_at.strftime("%b %d,%Y %H:%M:%S")
          end
        end
        div do
          span do
            "Date Confirmed :"
          end
          span do
            user.confirmed_at.strftime("%b %d,%Y %H:%M:%S") unless user.confirmed_at.nil?
          end
        end
        div do
          span do
            "Created By :"
          end
          span do
            user.create_by
          end
        end
      end
    end

    if user.mobile_apps
      panel "Apps submitted for sale", :class => "comments" do
        div do
          table do
            tr do
              th do
                "App Name"
              end
              th do
                "Type"
              end
              th do
                "No. of Attempts"
              end
              th do
                "Token"
              end
              th do
                "State"
              end
            end

            user.mobile_apps.each do |mobile_app|
              tbody do
                tr do
                  td do
                    link_to mobile_app.title, admin_mobile_app_path(mobile_app.id)
                  end
                  td do
                    mobile_app.label.upcase
                  end
                  td do
                    "TODO"# mobile_app.attempts
                  end
                  td do
                    mobile_app.verification_token
                  end
                  td do
                    mobile_app.state
                  end
                end
              end
            end
  
          end
        end
      end
    end

    if user.mobile_apps
      panel "Apps Sold", :class => "comments" do
        div do
          table do
            tr do
              th do
                "App Name"
              end
              th do
                "Type"
              end
              th do
                "Min Purchase Price"
              end
              th do
                "Sold Price"
              end
              th do
                "No. of bids"
              end
            end

            user.mobile_apps.each do |mobile_app|
              tbody do
                tr do
                  td do
                    "TODO"# link_to mobile_app.title, "#"
                  end
                  td do
                    "TODO"# mobile_app.label.upcase
                  end
                  td do
                    number_to_currency(mobile_app.minimum_purchase_price, :unit => "$")
                  end
                  td do
                    "TODO"# number_to_currency(mobile_app.minimum_purchase_price, :unit => "$")
                  end
                  td do
                    "TODO"
                  end
                end
              end
            end
  
          end
        end
      end
    end


    if user.mobile_apps
      panel "Apps purchased", :class => "comments" do
        div do
          table do
            tr do
              th do
                "App Name"
              end
              th do
                "Type"
              end
              th do
                "Price"
              end
            end

            user.mobile_apps.each do |mobile_app|
              tbody do
                tr do
                  td do
                    "TODO"# link_to mobile_app.title, "#"
                  end
                  td do
                    "TODO"# mobile_app.label.upcase
                  end
                  td do
                    "TODO"# number_to_currency(mobile_app.minimum_purchase_price, :unit => "$")
                  end
                end
              end
            end
  
          end
        end
      end
    end
    
    
    panel "Billing Details", :class => "comments" do
      "To be defined"
    end
    active_admin_comments
    
  end
  
  form :partial => "form"

end
