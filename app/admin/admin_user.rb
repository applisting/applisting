ActiveAdmin.register AdminUser do
  menu parent: 'Admin'

  actions :all, :except => [:destroy]
  
  controller do
    before_filter :set_current_admin_user
    
    def set_current_admin_user
      AdminUser.current = current_admin_user
    end
    
    def permitted_params
      params.permit admin_user: [:name, :email, :password, :password_confirmation, :created_by]
    end
  end
  
  batch_action "Activate Users", :priority => 1, :if => proc{ params[:scope] != "active" }   do |collection_selection|
    users = AdminUser.find(collection_selection)
    users.delete(current_admin_user)
    puts "users in activate"
    users.each do |user|
      user.activate! unless user.active?
    end
    redirect_to admin_admin_users_path, :notice =>"Users activated successfully."
  end

  batch_action "Deactivate Users", :priority => 2, :confirm => "Are you sure you want to deactivate?", :if => proc{ params[:scope] != "inactive" }  do |collection_selection|
    users = AdminUser.find(collection_selection)

    if users.include?(current_admin_user)
      redirect_to admin_admin_users_path, :notice =>"Self deactivate prohibited!"
    else
      users.delete(current_admin_user)  # Ensure that the self is not get deactivated
      users.each do |user|
        user.deactivate! unless user.inactive?
      end
      redirect_to admin_admin_users_path, :notice =>"Users deactivated successfully."
    end
  end
  

  filter :email, :filters =>[:equals]
  filter :name, :filters =>[:equals]
  filter :state, :as => :select, :collection => {"Inactive" => "inactive", "Active" => "active"}, :filters =>[:equals]
  filter :created_at, :label =>"Created Date"

  index download_links: [:csv] do
    selectable_column
    column :name
    column :email
    column :state
    column "Created Date", :created_at
    default_actions
  end

  filter :email

  form do |f|
    f.inputs "Admin Details" do
      f.input :name
      f.input :email
    end
    f.actions
  end
  
  show do |ad|
    attributes_table do
      row :name
      row :email
      row :state
      row :created_by
      row :created_at
    end
    active_admin_comments
  end

end
