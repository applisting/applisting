ActiveAdmin.register MobileApp do
  permit_params :user, :label, :website_url, :verification_token, :verified_at, :monetization, :description, :seller, :category, :updated, :size, :rating, :compatibility, :developer_website, :developer_apps, :privacy_policy, :app_support, :state, :tagline

  index download_links: [:csv] do
    selectable_column
    column :title
    column :user
    column :auction
    column :tagline
    column :state
    column :delayed_job
    column :website_url
    column :label
    column :verification_token
    column :verified_at
    column :seller
    column :category
    column :updated
    column :size
    column :rating
    column :developer_website
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :user
      f.input :label
      f.input :website_url
      f.input :verification_token
      f.input :verified_at
      f.input :monetization
      f.input :description
      f.input :seller
      f.input :category
      f.input :updated
      f.input :size
      f.input :rating
      f.input :compatibility
      f.input :developer_website
      f.input :developer_apps
      f.input :privacy_policy
      f.input :app_support
      f.input :state
      f.input :tagline
    end
    f.actions
  end

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
