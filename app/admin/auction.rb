ActiveAdmin.register Auction do

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  permit_params :title, :description, :minimum_price_usd, :begins_at, :ends_at, :sold
  index do
    selectable_column
    column :mobile_app
    column :title
    column :description
    column :minimum_price_usd
    column :begins_at
    column :ends_at
    column :sold
    column :emailed_winner_at
    column :created_at
    column :updated_at
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :description
      f.input :minimum_price_usd
      f.input :begins_at
      f.input :ends_at
      f.input :sold
    end
    f.actions
  end
  
end
