var intervalId;
var mediumKey = "";

$(function () {
  // Initialize the jQuery File Upload widget:
  // console.log($('#image_file').length)
	setUpload()
});

function setUpload(){
	if ($('#image_file').length > 0){
			$('#image_file').fileupload({
				dataType: 'text', 
				forceIframeTransport: false,
				url: '/screenshot_upload',
				dropZone: $('#dropzone')
		  });
			$('#image_file').bind('fileuploadsubmit', function (e, data) {
				var input = $('#screenshot_category');
				data.formData = {category: input.val()}
				if (!data.formData.category) {
					input.focus();
					return false;
				}
			});
			$('#image_file').bind('fileuploaddone', function (e, data) {
				var mediumKey = data.result
				$('#upload_status').html('Please wait...')
				intervalId = setInterval("checkMediumStatus('"+mediumKey+"')", 2000);
			});
			$('#image_file').bind('fileuploadprogress', function (e, data) {
				$('#upload_status').html('Please wait...')
			});
		  
		}
}

function checkMediumStatus(key) {
  $.ajax({
    url: '/screenshot_upload_status?key='+key,
	beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
    type: 'get',
    success: function(data) {
      var status = data.status;

      if(status=="processing") {
        //message = $('#'+field+'_outer').html();
        //message += "..";
        //$('#'+field+'_outer').html(message);

      } else if(status=="ready") {
        $('#upload_status').html("Image uploaded.")
        $li = $('<li />').attr('class', 'ui-state-default').data('id', data.key).html('<span class="remove_img">x</span><p><img src="'+data.thumb_url+'" />')
        $('#mobile_app_images_sortable').append($li)          
        populateSortableList()
        setImageRemovable($li)
        
        clearInterval(intervalId)

      } else if(status=='error'){
        $('#upload_status').html("<font color='red'>Uploading error!</font>");
        clearInterval(intervalId);
      }
    }
  })
}

function populateSortableList(){
  ids = []
  
  $('#mobile_app_images_sortable li').each(function(){
    ids.push($(this).data('id'))
  })
  
  // $('#mobile_app_image_ids').val(ids)
}

function setImageRemovable(li){
  $(li).find('span.remove_img').click(function(){
    id = $(li).data('id')
    $.ajax({
      type: 'delete',
      url: '/screenshot_delete?key='+id,
      success: function(data){
        if(data.status=='deleted'){
          $(li).fadeOut(300, function(){
            $(this).remove()
            populateSortableList()
          })
        }
      }
    })
  })
}