# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

add_fields = (link, association, content) ->
  @new_id = new Date().getTime()
  @regexp = new RegExp("new_" + association, "g")
  $(link).parent().before(content.replace(@regexp, @new_id))

remove_fields = (link, remove_tag) ->
  if remove_tag
   $(link).closest(".fields").remove()
   $(".screenshot-container").slideLeft()
  else
   $(link).prev("input[type=hidden]").val("1")
   $(link).closest(".fields").slideUp()

window.add_fields = add_fields
window.remove_fields = remove_fields

updateCountdown = ->  
  # 140 is the max message length
  if jQuery(".mobile-app-tagline").val()
   remaining = 100 - jQuery(".mobile-app-tagline").val().length
   jQuery(".countdown").text remaining + " characters remaining."


limitText = (field, maxChar) ->
  ref = $(field)
  val = ref.val()
  if val.length >= maxChar
    ref.val ->
      val.substr 0, maxChar
	
$(document).ready ->
	$(".remove-image").click ->
		$(this).parent().remove()
	
	$(".sortable").sortable()
	$("input.datepicker").datepicker({ dateFormat: 'dd-mm-yy' });
	updateCountdown()
	$(".mobile-app-tagline").change updateCountdown
	$(".mobile-app-tagline").keyup updateCountdown
	
	$(".mobile-app-tagline").on "keyup", ->
	  limitText this, 100