# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $(".mobile_app_filter").on "change", ->
    pathname = window.location.pathname
    window.location = pathname+"?"+"filter="+@value

  $('.list-row').each ->
    maxIconHeight = 0
    maxDescriptionHeight = 0
    maxTitleHeight = 0

    $(this).find('.list-app-icon-container').each( ->
      maxIconHeight = Math.max(maxIconHeight, $(this).height())
    ).height(maxIconHeight).end().find('.list-description').each( ->
      maxDescriptionHeight = Math.max(maxDescriptionHeight, $(this).height())
    ).height(maxDescriptionHeight).end().find('.list-title').each( ->
      maxTitleHeight = Math.max(maxTitleHeight, $(this).height())
    ).height(maxTitleHeight)
