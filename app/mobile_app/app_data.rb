module AppData
  class Profile
    # attr_accessor :id, :name, :url, :developer, :category, :logo_url, :short_description, :rating, :reviews, :price, :screenshoturls
    attr_accessor :description, :title, :seller_name, :primary_genre_name, :release_date, :version, :file_size, :rating, :developer_website,
    :supported_device, :developer_apps, :privacy_policy, :app_support, :screenshoturls
  end
end
