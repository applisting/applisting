class MobileAppBidMailer < ActionMailer::Base
  default from: ENV['FROM_EMAIL']
  
  def place_bid_email(bid)
     @user = bid.mobile_app.user
     @bid = bid
     mail(:to => @user.email, :subject => "New Bid Details")
   end
   
end
