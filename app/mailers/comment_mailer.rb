class CommentMailer < ActionMailer::Base
  default from: ENV['FROM_EMAIL']
  
  def comment_email(comment, mobile_app)
     @comment = comment
     mail(:to => mobile_app.user.email, :subject => "New Comment on #{mobile_app.title}")
   end
   
end
