class UserMailer < ActionMailer::Base
  default from: ENV['FROM_EMAIL']
  
  def welcome(user)
     @user = user
     mail(:to => user.email, :subject => "Welcome to App Listing")
  end

  def password_change(user,raw)
     @user = user
     @token = raw
     mail(:to => user.email, :subject => "App Listing - Change Password")
  end
   
end
