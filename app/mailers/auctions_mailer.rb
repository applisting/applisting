class AuctionsMailer < ActionMailer::Base
  default :from => ENV['FROM_EMAIL']
  default :bcc => AdminUser.active.map(&:email)

  def finalize_auction(mobile_app, auction, winning_user)
    @user = winning_user
    @mobile_app = mobile_app
    @auction = auction
    mail(:to => @user.email, :subject => "You have just won the Application #{@mobile_app.title} on AppListing")
  end

  def notify_seller(mobile_app, auction)
    @user = mobile_app.user
    @mobile_app = mobile_app
    @auction = auction
    mail(:to => @user.email, :subject => "Your Auction #{@mobile_app.title} has Ended on AppListing")
  end

  def payment_made(user, mobile_app, auction)
    @seller = mobile_app.user
    @buyer = user
    @mobile_app = mobile_app
    @auction = auction
    mail(:to => @seller.email, :subject => "Payment Received for #{@mobile_app.title} on AppListing")
  end
  
end
