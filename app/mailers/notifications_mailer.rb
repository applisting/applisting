class NotificationsMailer < ActionMailer::Base
  default :from => ENV['FROM_EMAIL']
  default :to => AdminUser.active.map(&:email)

  def new_message(message)
    @message = message
    mail(:subject => message.subject)
  end

  def app_verified(mobile_app)
    @mobile_app = mobile_app
    @user = @mobile_app.user
    mail(:to => @user.email, :subject => "App verified on App Listing")
  end
  
end
