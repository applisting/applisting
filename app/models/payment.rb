class Payment < ActiveRecord::Base
  belongs_to :user
  belongs_to :auction

  after_create :send_payment_made_notification

  protected

  def send_payment_made_notification
    AuctionsMailer.payment_made(self.user, self.auction.mobile_app, self.auction).deliver
  end
end
