class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable
  # attr_accessible :email, :password, :password_confirmation, :remember_me

  validates :name, :presence => true
  before_validation :temp_password
  after_create :set_created_by
  after_create :send_password_change
  after_update :audit

  scope :active, -> { where(:state => "active") }

  def temp_password
    if self.new_record?
      pw = Devise.friendly_token.first(8)
      logger.debug "password#{pw}"
      self.password = pw
      self.password_confirmation = pw
    end
  end

  def send_password_change
    raw, enc = Devise.token_generator.generate(self.class, :reset_password_token)
    self.reset_password_token   = enc
    self.reset_password_sent_at = Time.now.utc
    self.save(:validate => false)
    UserMailer.password_change(self,raw).deliver
  end

  state_machine :state, :initial => :inactive do
    # after_transition :do => :audit
    event :activate  do
      transition :inactive => :active
    end
    event :deactivate do
      transition :active => :inactive
    end

  end

  def set_created_by
    self.created_by = AdminUser.current.name if AdminUser.current
  end

  def self.current
    Thread.current[:admin_user]
  end

  def self.current=(user)
    Thread.current[:admin_user] = user
  end

  def audit
    if self.changed?
      comment = ActiveAdmin::Comment.new
      comment.resource = self
      comment.author = AdminUser.current rescue nil
      comment.namespace = "admin"
      msg = "Values Changed <br />"
      self.changes.each do |k, v|
        if k.upcase == "STATE" || k.upcase == "NAME" || k.upcase == "EMAIL"
          msg << "#{k.upcase}: #{v[0]} > #{v[1]} <br/>" unless k.eql?("updated_at")
          comment.body = "#{msg}"  rescue nil
          comment.save
        end
      end
    end
  end

  def is_admin?
    true
  end

  def active_for_authentication?
    super && self.is_active? # i.e. super && self.is_active
  end

  def is_active?
    self.state== "active" ? true : false
  end

  def inactive_message
    I18n.t("devise.failure.inactive")
  end
end
