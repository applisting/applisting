class User < ActiveRecord::Base
  
  has_many :mobile_apps
  has_many :mobile_app_bids
  has_many :auctions, :through => :mobile_apps
  has_many :won_auctions, :through => :mobile_app_bids, :source => :won_auction
  has_many :payments
  has_many :paid_auctions, :through => :payments, :source => :auction
  has_many :paid_mobile_apps, :through => :paid_auctions, :source => :mobile_app
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  # ROLES = [:buyer, :seller]

  # Setup accessible (or protected) attributes for your model
  # attr_accessible :name, :email, :password, :password_confirmation, :remember_me

  # validates :role, :presence => true, :inclusion => { :in => ROLES }
  validates :user_name, :presence => true, :uniqueness=>true
  scope :confirmed, -> { where('confirmed_at  is not null') }
  scope :not_confirmed, -> { where(confirmed_at: nil) }
  before_validation :set_created_by
  after_update :audit
  
  def set_created_by
    if AdminUser.current
      self.create_by = AdminUser.current.name
    else
      self.create_by = "Self Registration"
    end
  end
  
  def self.current
    Thread.current[:user]
  end
  
  def self.current=(user)
    Thread.current[:user] = user
  end

  def balanced_customer
    return Balanced::Customer.find(self.customer_uri) if self.customer_uri.present?
    customer = self.class.create_balanced_customer(:name => self.name, :email  => self.email)
    self.customer_uri = customer.uri
    self.save
    customer
  end

  def payment_information_present?
    self.customer_uri.present?
  end

  def self.create_balanced_customer(params = {})
    begin
      Balanced::Marketplace.mine.create_customer(
        :name   => params[:name],
        :email  => params[:email]
        )
    rescue
      raise 'There was an error adding a customer'
    end
  end
  
  
  def audit
    if self.changed?
      comment = ActiveAdmin::Comment.new
      comment.resource = self
      comment.author = User.current rescue nil
      comment.namespace = "admin"
      msg = "Values Changed <br />"
      self.changes.each do |k, v|
        if k.upcase == "NAME" || k.upcase == "EMAIL" || k.upcase == "COMMENT_NOTIFICATION_ENABLED"
          msg << "#{k.upcase}: #{v[0]} > #{v[1]} <br/>" unless k.eql?("updated_at")
          comment.body = "#{msg}"  rescue nil
          comment.save
        end
      end
    end
  end
  
  def is_admin?
    false
  end

  def confirm!
    UserMailer.welcome(self).deliver
    super
  end
  
end
