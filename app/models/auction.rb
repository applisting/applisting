class Auction < ActiveRecord::Base
  belongs_to :mobile_app
  belongs_to :winning_bid, :class_name => "MobileAppBid", :foreign_key => :winning_bid_id
  has_one :owner, :through => :mobile_app, :source => :user
  has_one :payment
  validates :description, :ends_at, :begins_at, :presence => true, :unless => :skip_validation
  validates :minimum_price_usd, :numericality => { :greater_than_or_equal_to => 0 }, :unless => :skip_validation
  validate :begins_at_before_ends_at, :unless => :skip_validation
  validate :ends_at_cannot_be_in_past, :unless => :skip_validation
  attr_accessor :check_val, :skip_validation

  def sell_price_usd
    #for now, just return the highest bid 
    self.mobile_app.mobile_app_bid.order("bid_value ASC").first.try(:bid_value) || self.minimum_price_usd
  end

  def started?
    self.begins_at && self.begins_at < Time.now
  end

  def ended?
    self.ends_at && self.ends_at < Time.now
  end

  def active?
    self.started? && !self.ended?
  end

  def state
    case
    when self.active?
      "active"
    when self.ended?
      "ended"
    when !self.started?
      "not yet started"
    when self.invalid?
      "incomplete data"
    else
      "other"
    end
  end

  private

  def ends_at_cannot_be_in_past
    errors.add(:ends_at, "cannot occur in the past") if (self.ends_at.present? && self.ends_at < Time.now)
  end

  def begins_at_before_ends_at
    errors.add(:begins_at, "needs to take place before the ends at time") if (self.begins_at.present? && self.ends_at.present? && (self.begins_at > self.ends_at))
  end
end
