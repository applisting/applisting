class MobileAppBid < ActiveRecord::Base
  belongs_to :mobile_app
  belongs_to :user
  has_one :won_auction, :class_name => "Auction", :foreign_key => :winning_bid_id
  
  validates :bid_value, :presence => true
  validates :bid_value, :numericality => {:only_integer => true}
  validates :mobile_app_id, :presence => true
  validate :place_bid_value

  after_create :send_place_bid_email

  STEP = 50
  
  def send_place_bid_email
    MobileAppBidMailer.place_bid_email(self).deliver
  end
  
  private
    def place_bid_value
      mobile_app = MobileApp.find(self.mobile_app_id)
      all_bids = mobile_app.mobile_app_bid
      if !all_bids.blank?
        if all_bids.maximum("bid_value").to_i+STEP > self.bid_value.to_i
          self.errors[:bid_value] << "needs to be at least $#{STEP} more than the last highest bid of $#{all_bids.maximum("bid_value").to_i}!"
        end
      elsif mobile_app.auction.minimum_price_usd.to_i > self.bid_value.to_i
        self.errors[:bid_value] << "needs to be greater than or equal to Minimum Purchase Price!"
      end
    end
  
end
