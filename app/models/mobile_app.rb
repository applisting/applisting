require 'verify_app'

class MobileApp < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper

  belongs_to :user

  has_many :screenshoturls, :dependent => :destroy
  has_many :screenshots, :dependent => :destroy
  has_many :mobile_app_views
  has_many :mobile_app_bid
  has_one :auction
  belongs_to :delayed_job, :class_name => Delayed::Job

  accepts_nested_attributes_for :screenshoturls, :allow_destroy => true
  accepts_nested_attributes_for :screenshots, :allow_destroy => true, :reject_if => proc { |attributes| attributes['screenshot'].blank? || attributes['category'].blank? }
  accepts_nested_attributes_for :auction

  attr_writer :current_step
  
  LABELS = ["android", "ios"]

  SCREENSHOT_CATEGORY_ORDER = {
    "app" => 0
  }

  STEPS = %w[app_info app_profile app_screenshots app_token_generate]

  before_destroy :check_for_bids
  before_destroy :destroy_associations

  before_validation :set_label
  before_validation :reset_app_data, :if => :app_id_changed?
  before_validation :skip_auction_validation, :unless => :auction_step?

  validates :label, :presence => true, :inclusion => { :in => LABELS }
  validates :app_id, :presence => true, :uniqueness => true
  
  validate :app_id_url
  # validate :build_app_data, :only => [:create]
  
  acts_as_commentable

  scope :pending_apps, -> { where(state: 'pending') }
  scope :published_apps, -> {includes(:auction).where("auctions.id IS NOT NULL").where(state: 'published')}
  scope :failed_apps, -> {where(state: 'failed')}

  scope :featured, -> { order("mobile_apps.created_at DESC") }
  scope :popular, -> { includes(:auction).joins(:mobile_app_views).group("mobile_apps.id, auctions.id").order("count(mobile_app_views.mobile_app_id) DESC") }
  scope :recently_sold, -> { includes(:auction).where("auctions.sold IS TRUE").order("auctions.ends_at DESC") }

  state_machine :state, :initial => :pending do
    before_transition :to => :published, :do => :set_verified_at
    after_transition :to => :published, :do => :send_notification

    event :publish do
      transition [:pending, :failed] => :published
    end
    event :pending do
      transition [:published, :failed] => :pending
    end
    event :failed do
      transition [:published, :pending] => :failed
    end
  end

  after_create :generate_verification_token
  after_create :verify_mobile_app

  def self.search(query)
    scoped.where("LOWER(mobile_apps.title) like ? or LOWER(mobile_apps.description) like ?","%#{query}%","%#{query}%")
  end

  def self.published_or_owned_by_user(id, user_id=nil)
    app = scoped.published_apps.find_by_id(id)
    if app.nil? && user_id.present?
      app = scoped.where(:user_id => user_id).find_by_id(id)
    end
    app.presence || raise(ActiveRecord::RecordNotFound)
  end

  def set_label
    if self.valid_android_url?
      self.label = "android"
    elsif self.valid_ios_url?
      self.label = "ios"
    end
  end

  def app_id_url
    unless self.valid_android_url? || self.valid_ios_url?
      errors.add(:app_id, "Invalid App or Play Store URL")
    end
  end

  def valid_android_url?
    self.app_id.starts_with? "https://play.google.com/store/apps"
  end

  def valid_ios_url?
    parts = self.app_id.partition('id')
    self.app_id.starts_with?("https://itunes.apple.com") && parts[0].include?('/itunes.apple.com/') && parts.size == 3 && parts.include?('id')
  end

  def android?
    self.label == "android"
  end

  def ios?
    self.label == "ios"
  end

  def all_screenshots
    @all_categories ||= self.screenshots | self.screenshoturls
  end

  def all_screenshots_categorized
    @all_screenshots_categorized ||= begin
                                       unsorted = self.all_screenshots.group_by do |s|
                                         if s.respond_to?(:category)
                                           s.category.presence || "app"
                                         else
                                           "app"
                                         end
                                       end
                                       Hash[ unsorted.sort_by { |k, v| SCREENSHOT_CATEGORY_ORDER[k] || 10000 } ]
                                     end
  end

  def generate_verification_token
    self.update_attributes(:verification_token => SecureRandom.hex(32))
  end

  def verify_mobile_app
    dj = Delayed::Job.enqueue(VerifyApp.new(self)) #if Delayed::Job.all.blank?
    self.update_attributes(:delayed_job_id => dj.id)
  end

  def ios_app_id
    regex = /id([0-9])+/
    id = regex.match(self.app_id).to_s
    if id.present?
      id = id.gsub("id","")
    end
    return id.to_i
  end

  def build_app_data

    app_data = AppData::Profile.new

    if self.android?

      doc = Nokogiri::HTML(open(self.app_id).read)

      if doc.css("div.id-app-orig-desc").blank? || doc.css("a.document-subtitle.primary").blank?
        self.errors.add(:app_id,"App data not found")
      else
        app_data.title = doc.css("div.document-title").blank? ? "" : doc.css("div.document-title").first.content.strip
        app_data.description = doc.css("div.id-app-orig-desc").blank? ? "" : doc.css("div.id-app-orig-desc").first.content.strip
        app_data.seller_name = doc.css("a.document-subtitle.primary").blank? ? "" : doc.css("a.document-subtitle.primary").first.content.strip
        app_data.primary_genre_name = doc.css("a.document-subtitle.category").blank? ? "" : doc.css("a.document-subtitle.category").first.content.strip
        app_data.release_date = doc.search('div.content')[0].content.blank? ? "" : doc.search('div.content')[0].content.strip
        app_data.version = doc.search('div.content')[3].content.blank? ? "" : doc.search('div.content')[3].content.strip
        app_data.file_size = doc.search('div.content')[1].content.blank? ? "" : doc.search('div.content')[1].content.strip
        app_data.rating = doc.search('div.content')[5].content.blank? ? "" : doc.search('div.content')[5].content.strip
        app_data.developer_website = doc.search('div.content a.dev-link').blank? ? "" : doc.search('div.content a.dev-link').first['href'] #TODO: needs more work here
        app_data.supported_device = doc.search('div.content')[4].blank? ? "" : doc.search('div.content')[4].content.strip
        app_data.developer_apps = ""
        app_data.privacy_policy = doc.search('div.content a.dev-link')[2].blank? ? "" : doc.search('div.content a.dev-link')[2]['href'] #TODO: needs more work here
        app_data.app_support = ""
        screenshoturls = []

        doc.css("img.screenshot").each do |link|
          screenshoturls << link['src']
        end
        app_data.screenshoturls = screenshoturls

      end
    elsif self.ios?
      itunes_data = ITunesSearchAPI.lookup(:id => self.ios_app_id)
      if itunes_data.nil? || itunes_data["description"].blank? || itunes_data["sellerName"].blank?
        self.errors.add(:app_id,"App data not found")
      else
        app_data.title = itunes_data["trackName"]
        app_data.description = itunes_data["description"]
        app_data.seller_name = itunes_data["sellerName"]
        app_data.primary_genre_name = itunes_data["primaryGenreName"]
        app_data.release_date = itunes_data["releaseDate"].blank? ? "" : itunes_data["releaseDate"]
        app_data.version = itunes_data["version"]
        app_data.file_size = "#{(itunes_data["fileSizeBytes"].to_f/(1024*1024)).round(2)} MB"
        app_data.rating = itunes_data["contentAdvisoryRating"]
        app_data.developer_website = itunes_data["sellerUrl"]
        app_data.supported_device = itunes_data["supportedDevices"].join(',')
        app_data.developer_apps = ""
        app_data.privacy_policy = ""
        app_data.app_support = ""
        screenshoturls = []

        if !itunes_data["screenshotUrls"].blank?
          itunes_data["screenshotUrls"].each do |link|
            screenshoturls << link
          end

        else
          
          itunes_data["ipadScreenshotUrls"].each do |link|
            screenshoturls << link
          end
          
        end
        app_data.screenshoturls = screenshoturls
      end

    end
    return app_data
  end

  def current_step
    @current_step || STEPS.first
  end

  def next_step
    self.current_step = STEPS[STEPS.index(current_step)+1]
  end

  def previous_step
    self.current_step = STEPS[STEPS.index(current_step)-1]
  end

  def first_step?
    current_step == STEPS.first
  end

  def second_step?
    current_step == STEPS.second
  end
  
  def third_step?
    current_step == STEPS.third
  end

  def fourth_step?
    current_step == STEPS.fourth
  end
  
  def last_step?
    current_step == STEPS.last
  end

  def reset_app_data
    self.pending unless self.pending?
    self.title = nil
    self.description = nil
    self.seller = nil # app_data.seller_name
    self.category = nil # app_data.primary_genra_name
    self.updated = nil # app_data.release_date
    self.version = nil
    self.size = nil # app_data.file_size
    self.rating = nil
    self.developer_website = nil
    self.compatibility = nil # app_data.supported_device
    self.developer_apps = nil
    self.privacy_policy = nil
    self.app_support = nil
    screenshoturls = []
  end

  def skip_auction_validation
    self.auction.skip_validation = true if self.auction.present?
  end

  def auction_step?
    current_step == "app_profile"
  end

  def check_for_bids
    if self.mobile_app_bid.any?
      errors[:base] << "Cannot delete app which already has bids"
      return false
    end
  end

  def destroy_associations
    self.screenshoturls.destroy unless self.screenshoturls.blank?
    self.screenshots.destroy unless self.screenshots.blank?
    self.mobile_app_views.destroy unless self.mobile_app_views.blank?
    self.mobile_app_bid.destroy unless self.mobile_app_bid.blank?
  end

  def set_verified_at
    self.verified_at = Time.now
  end

  def send_notification
    NotificationMailer.app_verified(self).deliver
  end
  
  # def format_minimum_purchase_price
  #   self.minimum_purchase_price = (number_with_precision(self.minimum_purchase_price, :precision => 2))
  # end
  # handle_asynchronously :verify_mobile_app, :priority => 20, :run_at => Proc.new { 2.seconds.from_now  }

end
