class Comment < ActiveRecord::Base

  include ActsAsCommentable::Comment

  belongs_to :commentable, :polymorphic => true

  default_scope -> { order('created_at ASC') }

  # NOTE: install the acts_as_votable plugin if you
  # want user to vote on the quality of comments.
  #acts_as_voteable

  # NOTE: Comments belong to a user
  belongs_to :user

  after_create :send_email_to_admin

  def send_email_to_admin
    mobile_app = MobileApp.find_by_id(self.commentable_id)
    if mobile_app.user.id != self.user_id && mobile_app.user.comment_notification_off != true
      CommentMailer.comment_email(self,mobile_app).deliver
    end  
  end
end
