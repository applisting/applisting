class Screenshot < ActiveRecord::Base
  # LABELS = [:in_app, :traffic, :revenue]

  # validates :label, :presence => true, :inclusion => { :in => LABELS }
  
  belongs_to :mobile_app
  
  has_attached_file :screenshot, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => ""

  def url
    self.screenshot.url
  end
end
