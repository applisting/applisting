class Screenshoturl < ActiveRecord::Base
  belongs_to :mobile_app
  
  validates_uniqueness_of :url
end
