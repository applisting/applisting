class ContactsController < ApplicationController
  skip_before_filter :authenticate_user!
  def new
    @message = Message.new
  end
  
  def create
    @message = Message.new(params[:message])
    
    if @message.valid?
      NotificationsMailer.new_message(@message).deliver
      redirect_to(root_path, :notice => "Your message has been sent - App Listing support will get back to you shortly.")
    else
      flash.now.alert = "Name, Email, Subject and Message can't be blank"
      render :new
    end
  end
end
