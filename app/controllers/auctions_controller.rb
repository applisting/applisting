class AuctionsController < ApplicationController
  skip_before_filter :authenticate_user!, :only => :show
  after_filter :update_app_views, :only => :show
  
  def show
    @mobile_app = MobileApp.published_or_owned_by_user(params[:id], current_user.try(:id))
    @auction = @mobile_app.auction
    @comments = @mobile_app.comments.recent.limit(100).all
    if current_user
      @mobile_app_bid = MobileAppBid.new()
      @comment = @mobile_app.comments.new()
    end
  end

  def place_bid
    @mobile_app = MobileApp.find params[:mobile_app_bid][:mobile_app_id]
    @auction = @mobile_app.auction
    if @auction.present? && @auction.ends_at >= Time.now
      if current_user.payment_information_present?
        @mobile_app_bid = current_user.mobile_app_bids.new(mobile_app_bid_params)
        if @mobile_app_bid.save
          flash[:success] = "Your bid placed successfully!"
          redirect_to auction_path(@mobile_app)
        else
          flash[:error] = "There were problems with your bid: #{@mobile_app_bid.errors.full_messages}"
          redirect_to auction_path(@mobile_app)
        end
      else
        flash[:error] = "You need to enter your payment information before you can bid on an auction. <a href='/users/payment'>Click here to add your account information</a>".html_safe
        redirect_to auction_path(@mobile_app)
      end
    else
      flash[:error] = "This auction has now closed. Your bid was not recorded successfully."
      redirect_to auction_path(@mobile_app)
    end
  end

  def bids
    @mobile_app = current_user.mobile_apps.find(params[:auction_id])
    @auction = @mobile_app.auction
    @bids = @mobile_app.mobile_app_bid
  end

  def create_comments
    @mobile_app = MobileApp.find(params[:comment][:mobile_app_id])
    @mobile_app.comments.create(:comment => params[:comment][:comment], :user_id => current_user.id)
    flash[:success] = "Your comment placed successfully!"
    redirect_to auction_path(@mobile_app)
  end

  private

  def mobile_app_bid_params
    params.require(:mobile_app_bid).permit(:mobile_app_id, :user_id, :bid_value)
  end
end
