class UsersController < ApplicationController
  skip_before_filter :authenticate_user!, :only => :show
  before_filter :confirm_correct_user, :only => [:edit, :update, :payment, :payment_post]
  
  def show
    @user = User.find(params[:id])
  end

  def edit
  end
  
  def update
    if params[:user][:email] && @user.confirmed? && (@user.email != params[:user][:email])
      @user.confirmed_at = nil
      Devise::Mailer.confirmation_instructions(@user, nil).deliver
      @user.save(:validate => false)
    end
    if @user.update_attributes(user_params)
      if @user.email_changed?
        sign_out @user
        flash[:notice] = "Profile updated successfully."
        redirect_to root_path
      else
        flash[:notice] = "Profile updated successfully."
        redirect_to user_path(params[:id])
      end
    else
      flash[:error] = "Profile update error."
      render :action => 'edit'
    end
    
  end

  def payment
    if @user.customer_uri.present?
      customer_account = Balanced::BankAccount.find(@user.customer_uri)
      bank_account = Balanced::BankAccount.find(customer_account.bank_accounts_uri).attributes["items"].first
    elsif @user.bank_account_uri.present?
      bank_account = Balanced::BankAccount.find(@user.bank_account_uri).attributes
    end
    if bank_account.present?
      @bank_name = bank_account["bank_name"]
      @account_number = bank_account["account_number"]
      @routing_number = bank_account["routing_number"]
      @account_type = bank_account["type"]
    end
  end

  def payment_post
    balanced_user = @user.balanced_customer #Retrieve or create balanced customer
    balanced_user.add_bank_account(params[:bank_account_uri]) #Associate bank account with customer
    @user.bank_account_uri = params[:bank_account_uri]
    if @user.save!
      flash[:notice] = "Account successfully added"
    else
      flash[:error] = "There was an error with your bank account information. Please try again."
    end
    redirect_to payment_path
  end
  
  private


  def confirm_correct_user
    if user_signed_in? && current_user.present?
      @user = current_user
    else
      flash[:error] = "Access Denied"
      redirect_to root_url 
    end
  end


  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :user_name, :street_address, :zipcode, :comment_notification_off)
  end
  
  
end
