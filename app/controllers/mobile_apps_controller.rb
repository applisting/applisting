class MobileAppsController < ApplicationController
  skip_before_filter :authenticate_user!, :only => [:index, :show]
  before_filter :disallow_modifying_completed_auctions, :only => [:edit, :update]

  def new
    # remove_session
    if params[:cancel_button]
      remove_session
      redirect_to mobile_apps_path and return
    end
    @mobile_app = MobileApp.new
  end

  def create
    if params[:cancel_button]
      remove_session
      redirect_to mobile_apps_path and return
    end
    @mobile_app = MobileApp.new(mobile_app_params)
    @mobile_app.current_step = current_step
    @mobile_app.user = current_user

    if @mobile_app.errors.blank? && @mobile_app.save
      @app_data = @mobile_app.build_app_data
      @mobile_app.next_step
      session[:mobile_app] = @mobile_app
      redirect_to edit_mobile_app_path(@mobile_app, :step => @mobile_app.current_step) and return
    end
    render :action => :new
  end

  def edit
    @mobile_app = current_user.mobile_apps.find(params[:id])
    @auction = @mobile_app.auction || @mobile_app.build_auction
    @mobile_app.current_step = current_step
    if ['app_profile','app_screenshots'].include?(current_step)
      @app_data ||= @mobile_app.build_app_data

      @mobile_app.screenshots.build if @mobile_app.screenshots.blank?
      @app_data.screenshoturls.each do |url|
        @mobile_app.screenshoturls.build(:url => url) if Screenshoturl.find_by_url(url).blank?
      end
    end
  end

  def update
    if params[:cancel_button]
      remove_session
      redirect_to user_path(current_user) and return
    end
    @mobile_app = current_user.mobile_apps.find(params[:id])
    @auction = @mobile_app.auction
    @mobile_app.current_step = current_step

    Rails.logger.info "STEP: #{current_step}"
    case current_step
    when 'app_info'
      @mobile_app.update_attributes(mobile_app_params)
      if @mobile_app.errors.blank?
        @app_data = @mobile_app.build_app_data
        @mobile_app.next_step
      else
        Rails.logger.info "Errors: #{@mobile_app.errors.messages}"
        flash[:alert] = "There was a problem saving the app."
        render :action => :new and return
      end
    when 'app_profile'
      @app_data ||= @mobile_app.build_app_data
      set_form_data
      if params[:back_button]
        @mobile_app.previous_step
      else
        if @mobile_app.update_attributes(mobile_app_params)
          @mobile_app.screenshots.build if @mobile_app.screenshots.blank?

          @app_data.screenshoturls.each do |url|
            @mobile_app.screenshoturls.build(:url => url)
          end
          @mobile_app.next_step
        else
          render :action => :edit and return
        end
      end
    when 'app_screenshots'

      if params[:back_button]
        @mobile_app.previous_step
        @app_data ||= @mobile_app.build_app_data
      elsif @mobile_app.valid? && @mobile_app.update_attributes(mobile_app_params)
        @mobile_app.next_step
      end

    when 'app_token_generate'
      if params[:back_button]
        @mobile_app.previous_step
        @app_data ||= @mobile_app.build_app_data
      elsif !@mobile_app.verification_token.blank?
        session[:mobile_app_step] = session[:mobile_app_params] = session[:mobile_app] = nil
        if @mobile_app.update_attributes(mobile_app_auction_params)
          flash[:notice] = "App has been listed for sale successfully"
          redirect_to auction_path(@mobile_app) and return
        end
      else
        @mobile_app.previous_step
      end
    end
    

    redirect_to edit_mobile_app_path(@mobile_app, :step => @mobile_app.current_step)
  end

  def index
    filter_criteria = params[:filter] ? params[:filter] : "most_popular"
    case filter_criteria
    when "most_popular"
      @mobile_apps = MobileApp.includes(:mobile_app_views).where(:user_id => current_user.id).map{|a| [a, a.mobile_app_views.size]}.sort_by{|a| a.last}.reverse.collect{|a| a.first}
    when "purchase_price_high_to_low"
      @mobile_apps = MobileApp.where(:user_id => current_user.id).order('minimum_purchase_price asc')
    when "purchase_price_low_to_high"
      @mobile_apps = MobileApp.where(:user_id => current_user.id).order('minimum_purchase_price desc')
    when "most_recently_sold"
      #TODO
      @mobile_apps = [] 
    when "ending_soon"
      #TODO
      @mobile_apps = []       
    end

  end

  def screenshot_upload
    @mobile_app = current_user.mobile_apps.find(session[:mobile_app][:id])
    @mobile_app.update_attributes(mobile_app_screenshot_params)
    render :text => @mobile_app.screenshots.last.id # @mobile_app.screenshots.last.id
  end

  def screenshot_upload_status
    key = params[:key].to_i
    if Screenshot.exists?(key)
      image = Screenshot.find(key)
      render :json => { :key => image.id,
                        :status => 'ready',
                        :thumb_url => image.screenshot.url(:thumb),
                        :medium_url => image.screenshot.url(:medium)
                        }
    else
      render :json => { :status => 'error' }
    end

  end

  def screenshot_delete
    screenshot = screenshot.find(params[:key])
    mobile_app = current_user.mobile_apps.find(screenshot.mobile_app_id)
    if screenshot.destroy
      status = 'deleted'
    else
      status = 'error'
    end
    render :json => { :status => status }
  end

  def place_bid
    @mobile_app = MobileApp.find(params[:id])
    @mobile_app.mobile_app_bid << MobileAppBid.find_or_create(:bid_value => mobile_app_params["mobile_app_bid_attributes"]["bid_value"],:user_id => current_user.id )
  end
  
  def create_comments
    @mobile_app = MobileApp.find(params[:comment][:mobile_app_id])
    @mobile_app.comments.create(:comment => params[:comment][:comment], :user_id => current_user.id)
    redirect_to mobile_app_path(@mobile_app)
  end

  private

  def mobile_app_params
    params.require(:mobile_app).permit(:label, :app_id, :title, :description, :seller, :category, :updated, :version,
                                       :size, :rating, :compatibility, :developer_website, :state, :tagline,
                                       :developer_apps, :privacy_policy, :app_support, :minimum_purchase_price,:_destroy,
                                       :screenshoturls_attributes => [:id, :url,:_destroy],
                                       :screenshots_attributes => [:id, :screenshot, :category,:_destroy],
                                       :mobile_app_bid_attributes => [:id, :bid_value,:destroy],
                                       :auction_attributes => [:id, :title, :description, :begins_at, :ends_at, :minimum_price_usd])
    # params.fetch(:mobile_app, {})
  end


  def mobile_app_screenshot_params
    hash_key = params["mobile_app"]["screenshots_attributes"].first[0]
    if params["mobile_app"]["screenshots_attributes"][hash_key]["category"].nil?
      params["mobile_app"]["screenshots_attributes"][hash_key].deep_merge!(:category => params[:category])
    end
    params.require(:mobile_app).permit(:screenshots_attributes => [:id,:screenshot, :category])
  end

  def mobile_app_auction_params
    params.require(:mobile_app).permit(:auction_attributes => [:title, :description, :begins_at, :ends_at, :minimum_price_usd])
  end

  def remove_session
    session[:mobile_app_step] = nil
    session[:mobile_app_params] = nil
    session[:mobile_app] = nil

    if params[:id] && MobileApp.find(params[:id])
      @mobile_app = MobileApp.find(params[:id])
      if @mobile_app.destroy
        flash[:notice] = "App removed"
      else
        flash[:alert] = "App could not be removed. #{@mobile_app.errors.full_messages.join(". ")}."
      end
    end
  end
  
  def set_form_data
    @app_data.title = mobile_app_params[:title]
    @app_data.description = mobile_app_params[:description]
    @app_data.seller_name = mobile_app_params[:seller]
    @app_data.primary_genre_name = mobile_app_params[:category]
    @app_data.release_date = mobile_app_params[:updated]
    @app_data.version = mobile_app_params[:version]
    @app_data.file_size = mobile_app_params[:size]
    @app_data.rating = mobile_app_params[:rating]
    @app_data.developer_website = mobile_app_params[:developer_website]
    @app_data.supported_device = mobile_app_params[:compatibility]
    @app_data.developer_apps = mobile_app_params[:developer_apps]
    @app_data.privacy_policy = mobile_app_params[:privacy_policy]
    @app_data.app_support = mobile_app_params[:app_support]
  end

  def current_step
    params[:step].presence || MobileApp::STEPS.first
  end

  def disallow_modifying_completed_auctions
    @mobile_app = current_user.mobile_apps.find(params[:id])
    if @mobile_app.auction.present? && @mobile_app.auction.ended?
      flash[:error] = "This Auction has ended. You cannot modify it."
      redirect_to user_path(current_user)
    end
  end
end
