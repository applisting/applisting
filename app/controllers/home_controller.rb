class HomeController < ApplicationController
  skip_before_filter :authenticate_user!, :only => :index

  def index
    @featured = MobileApp.published_apps.featured.limit 3
    @popular = MobileApp.published_apps.popular.limit 3
    @recently_sold = MobileApp.published_apps.recently_sold.limit 3
  end

  def search
    @mobile_apps = MobileApp.published_apps.search(params[:q])
    if params[:platform].present?
      @mobile_apps = @mobile_apps.where(:label => params[:platform])
    end
  end

end
