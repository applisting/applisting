class PaymentsController < ApplicationController
  def index
    @paid_auctions = current_user.paid_auctions
    @unpaid_auctions = Auction.where(:winning_bid_id => current_user.mobile_app_bids.pluck(:id)).where("sold IS NULL or sold IS FALSE")
  end

  def show
    @payment = current_user.payments.find(params[:id])
    @auction = @payment.auction
    debit = Balanced::Debit.find(@payment.debit_uri)
    @last_four = debit.source.last_four
  end

  def pay
    @auction = current_user.won_auctions.find(params[:id])
    @user = current_user
    if @user.customer_uri.present?
      customer_account = Balanced::BankAccount.find(@user.customer_uri)
      bank_account = Balanced::BankAccount.find(customer_account.bank_accounts_uri).attributes["items"].first
    elsif @user.bank_account_uri.present?
      bank_account = Balanced::BankAccount.find(@user.bank_account_uri).attributes
    end
    if bank_account.present?
      @bank_name = bank_account["bank_name"]
      @account_number = bank_account["account_number"]
      @routing_number = bank_account["routing_number"]
      @account_type = bank_account["type"]
    else
      flash[:error] = "You need to enter your payment information before you can pay for an auction. <a href='/users/payment'>Click here to add your account information</a>".html_safe
      redirect_to auction_payments_path
    end
  end

  def pay_post
    @user = current_user
    @auction = current_user.won_auctions.find(params[:id])
    if @user.customer_uri.present?
      user_account = Balanced::Customer.find(@user.customer_uri)
    else
      user_account = @user.balanced_customer
    end
    if params[:card_uri].present?
      user_account.add_card(params[:card_uri])
      auction_owner_account = Balanced::BankAccount.find(@auction.owner.customer_uri)

      winning_bid_value = @auction.winning_bid.bid_value * 100
      @payment = @user.payments.where(:amount => winning_bid_value, :auction_id => @auction.id).first_or_initialize
      if @payment.new_record?
        debit = user_account.debit(:amount => winning_bid_value, :description => "App Listing - #{@auction.mobile_app.title}", :on_behalf_of => auction_owner_account)
        credit = auction_owner_account.credit(:amount => winning_bid_value, :description => "App Listing - #{@auction.mobile_app.title} - #{@user.name}(#{@user.email})" )
        @payment.credit_uri = credit.uri
        @payment.debit_uri = debit.uri
        @auction.sold = true
        if @payment.save! && @auction.save!(:validate => false)
          flash[:notice] = "You have successfully bought #{@auction.mobile_app.title} for $#{(winning_bid_value/100).to_i}"
          redirect_to auction_payments_path
        else
          flash[:error] = "Your payment was not successful. Please try again later."
          render :pay
        end
      else
        flash[:error] = "You've already fully paid for this application."
        render :pay
      end
    else
      flash[:error] = "Something went wrong. Please try again later.".html_safe
      redirect_to auction_payments_path
    end
  end
end
