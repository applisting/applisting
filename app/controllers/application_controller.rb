class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :update_sanitized_params, if: :devise_controller?

  before_filter :store_location, if: :devise_controller?, only: [:new]
  before_filter :authenticate_user!

  def update_sanitized_params
    devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:user_name, :name, :email, :password, :password_confirmation)}
  end

  # def after_sign_in_path_for(resource)
  #   if resource.is_admin?
  #     admin_root_path
  #   else
  #     new_mobile_app_path
  #   end
  # end


  def after_sign_in_path_for(resource)
    stored_location_for(resource) ||
    if resource.is_admin?
      admin_root_path
    else
      home_index_path
    end

  end

  private

  #   create mobile app view
  def update_app_views
    if MobileAppView.where(:mobile_app_id => params[:id], :request_ip => request.ip).blank?
      @mobile_app.mobile_app_views << MobileAppView.new(:request_ip => request.ip)
    end
  end

  def store_location
    session[:user_return_to] = request.protocol + ENV["DOMAIN"] + params[:return_to] if params[:return_to].present?
  end
end
