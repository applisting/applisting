require 'spec_helper'

describe "user pages", :type => :feature do
  
  it "allows signup user" do
    visit new_user_registration_path
    fill_in "name",                  :with => "Alindeman Example"
    fill_in "user_name",             :with => "alindeman"
    fill_in "Email",                 :with => "alindeman@example.com"
    fill_in "user[password]",              :with => "ilovegrapes"
    fill_in "user[password_confirmation]", :with => "ilovegrapes"
    click_button "Create Account"
    page.should have_content("A message with a confirmation link has been sent to your email address. Please open the link to activate your account.")
  end

  it "does not allows signup user" do
    visit new_user_registration_path
    fill_in "name",                  :with => "Alindeman Example"
    fill_in "user_name",             :with => "alindeman"
    fill_in "Email",                 :with => "alindeman@example.com"
    fill_in "user[password]",              :with => "ilovegrapes"
    fill_in "user[password_confirmation]", :with => "1111ilovegrapes"
    click_button "Create Account"
    page.should have_content("doesn't match Password")
  end

  it "does not allows signup user witout user name" do
    visit new_user_registration_path
    fill_in "name",                  :with => "Alindeman Example"
    fill_in "user_name",             :with => "alindeman"
    fill_in "user[password]",              :with => "ilovegrapes"
    fill_in "user[password_confirmation]", :with => "ilovegrapes"
    click_button "Create Account"
    page.should have_content("can't be blank")
  end

  it "allows users to sign in after they have registered" do
    user = FactoryGirl.build(:user)
    user.confirm!
    visit "/users/sign_in"
    fill_in "Email",    :with => "alindeman@example.net"
    fill_in "Password", :with => "ilovegrapes"
    click_button "Sign in"
    page.should have_content("Sell the App")
  end

  # it "allows users to get change password email" do
  #   user = FactoryGirl.build(:user)
  #   p user.email
  #   visit "/users/password/new"
  #   fill_in "user[email]",    :with => user.email
  #   click_button "Send me reset password instructions"
  #   page.should have_content("You will receive an email with instructions about how to reset your password in a few minutes.")
  # end
  
  # it "allows users to ask to resend confirmation email" do
  #   user = FactoryGirl.build(:user)
  #   visit "/users/confirmation/new"
  #   fill_in "Email",    :with => user.email
  #   click_button "Resend confirmation instructions"
  #   page.should have_content("You will receive an email with instructions about how to confirm your account in a few minutes")
  # end
   
end