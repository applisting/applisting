require 'spec_helper'
describe "routing to mobile app info" do
  it "routes /mobile_apps/new to mobile_apps#new " do
    expect(:get => "/mobile_apps/new").to route_to(
      :controller => "mobile_apps",
      :action => "new",
    )
  end

  it "routes /mobile_apps/ to mobile_apps#index " do
    expect(:get => "/mobile_apps/").to route_to(
      :controller => "mobile_apps",
      :action => "index",
    )
  end

  it "routes /mobile_apps/app_url to mobile_apps#show " do
    expect(:get => "/mobile_apps/app_url").to route_to(
      :controller => "mobile_apps",
      :action => "show",
      :id => "app_url"
    )
  end
  
  it "routes /mobile_apps to mobile_apps#create " do
    expect(:post => "/mobile_apps").to route_to(
      :controller => "mobile_apps",
      :action => "create",
    )
  end

  # it "does not expose a list of profiles" do
  #   expect(:get => "/profiles").not_to be_routable
  # end
end