require 'spec_helper'

describe MobileAppsController do
  include Devise::TestHelpers
  describe "adding mobile app to app auction" do
    before(:all) do
      MobileApp.destroy_all
      User.destroy_all
    end

    it "should respond with status 200" do
      user = FactoryGirl.build(:user)
      user.confirm!

      sign_in user

      get :new
      expect(response).to be_success
      expect(response.status).to eq(200)
      # user.destroy
    end

    it "should create a mobile app with given url, verification token should not be nil and redirect to next step" do
      user = FactoryGirl.build(:user)
      user.confirm!
    
      sign_in user
      session[:mobile_app_params] = {}
      post :create, :mobile_app => { :label => "android", :app_id => "https://play.google.com/store/apps/details?id=com.ea.game.dragonage_na" }
    
      assigns[:mobile_app].should be_valid
       assigns[:mobile_app]["verification_token"].should_not be_nil
      response.should redirect_to(edit_mobile_app_path(assigns[:mobile_app].id))
    end

    # it "index action should redirect to app profile page" do
    #   get :new
    # 
    #   response.should redirect_to("/mobile_apps/app_profile")
    # end
    #
    # it "should fetch app data from app store" do
    #   MobileApp.create(:label => "ios", :app_id => "327630330")
    #   session[:mobile_app_id] = "327630330"
    #
    #   get :show, id: :app_profile
    #
    #   assigns[:app_data].should_not == nil
    #   assigns[:app_data]["artistName"].should == "Dropbox"
    #
    # end
    #
    # it "should fill up the profile info fetched from app store" do
    #   MobileApp.create(:label => "ios", :app_id => "327630330")
    #   session[:mobile_app_id] = "327630330"
    #   app_data = ITunesSearchAPI.lookup(:id => "327630330")
    #
    #   put :update, id: :app_profile, :mobile_app => { :label => "ios", :app_id => "327630330", :title => app_data["artistName"], :description => app_data["description"],
    #     :seller => app_data["sellerName"], :category => app_data["primaryGenreName"], :updated => app_data["releaseDate"], :version => app_data["version"], :size => app_data["fileSizeBytes"],
    #     :rating => app_data["averageUserRating"], :compatibility => "", :developer_website => app_data["sellerUrl"], :developer_apps => "", :privacy_policy => "", :app_support => "",
    #     :minimum_purchase_price => "" }
    #
    #   response.should redirect_to("/mobile_apps/app_screen_shots")
    # end
    #
    # it "should not create an app without valid url" do
    #   post :create, :mobile_app => { :label => "android", :app_id => "https://play.goooogle.com/store/apps/details?id=com.ea.game.dragonage_na" }
    #
    #   assigns[:mobile_app].should have(1).errors
    #   assigns[:mobile_app].errors[:app_id][0].should eq("Invalid Android app URL")
    # end
    #
    # it "should not create an app with missing description or sellar name" do
    #   post :create, :mobile_app => { :label => "android", :app_id => "https://play.google.com/store/apps" }
    #
    #   assigns[:mobile_app].should have(1).errors
    #   assigns[:mobile_app].errors[:app_id][0].should eq("App data not found")
    # end
  end

end
