require 'spec_helper'

describe PaymentsController do

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      get 'show'
      response.should be_success
    end
  end

  describe "GET 'pay'" do
    it "returns http success" do
      get 'pay'
      response.should be_success
    end
  end

end
