require 'spec_helper'

describe HomeController do

  before :each do
    @user = FactoryGirl.build(:user)
    @mobile_app = FactoryGirl.build(:mobile_app, :label => "android", :app_id => "https://play.google.com/store/apps/details?id=com.ea.game.dragonage_na", :user_id => @user.id, :minimum_purchase_price => "1000")
    @mobile_app1 = FactoryGirl.build(:mobile_app, :label => "android", :app_id => "https://play.google.com/store/apps/details?id=net.nurik.roman.dashclock", :user_id => @user.id, :minimum_purchase_price => "1001")
    @mobile_app.save
    @mobile_app1.save
  end

  it "returns http success" do
    get 'index'
    response.should be_success
  end
  
  it "returns http success" do
    get :index, {:params => {:filter => 'most_popular'}}
    controller.params[:params][:filter].should eql 'most_popular'
  end
  
  # it "returns http success" do
  #   get :index, {:params => {:filter => 'most_popular'}}
  #   b = assigns(:mobile_apps)
  #   ap k.first.inspect
  #   controller.params[:params][:filter].should eql 'most_popular'
  # end

  it "returns http success" do
    get :index, {:params => {:filter => 'purchase_price_high_to_low'}}
    k = assigns(:mobile_apps)
    k.first.minimum_purchase_price.should eql "1001.00"
  end
  
  # it "returns http success" do
  #   get :index, {:params => {:filter => 'purchase_price_low_to_high'}}
  #   r = assigns(:mobile_apps)
  #   r.first.minimum_purchase_price.should eql "1000.00"
  # end
  
  it "returns http success" do
    get :index, {:params => {:filter => 'purchase_price_high_to_low'}}
    controller.params[:params][:filter].should eql 'purchase_price_high_to_low'
  end
  
  it "returns http success" do
    get :index, {:params => {:filter => 'purchase_price_low_to_high'}}
    controller.params[:params][:filter].should eql 'purchase_price_low_to_high'
  end
  
  it "returns http success" do
    visit auction_path(@mobile_app)
    response.should be_success
  end
  
  it "returns http success" do
    visit auction_path(@mobile_app)
    response.should be_success
  end
  
  it "placing comment should be raise error" do
    user = FactoryGirl.build(:user)
    user.confirm!
    sign_in user
    post :create_comments, :comment => {:mobile_app_id => @mobile_app.id, :comment => "Testing Comment"}
    flash[:success].should.eql?("Your comment placed successfully!")
  end
  
  it "placing first bid less to minimum purchase price should be raise error" do
    user = FactoryGirl.build(:user)
    user.confirm!
    sign_in user
    post :place_bid, :mobile_app_bid => {:bid_value => 100, :mobile_app_id => @mobile_app.id}
    flash[:error].should.eql?("Error!!! Please check bid value before you place a bid")
  end
  
  it "placing first bid equal to minimum purchase price should be success" do
    user = FactoryGirl.build(:user)
    user.confirm!
    sign_in user
    post :place_bid, :mobile_app_bid => {:bid_value => 1000, :mobile_app_id => @mobile_app.id}
    flash[:success].should.eql?("Your bid placed successfully!")
  end
  
  it "placing first bid $ 1 greater than to minimum purchase price should be success" do
    user = FactoryGirl.build(:user)
    user.confirm!
    sign_in user
    post :place_bid, :mobile_app_bid => {:bid_value => 1001, :mobile_app_id => @mobile_app.id}
    flash[:success].should.eql?("Your bid placed successfully!")
  end
  
  it "placing second bid less to first bid should be raise error" do
    user = FactoryGirl.build(:user)
    user.confirm!
    sign_in user
    post :place_bid, :mobile_app_bid => {:bid_value => 1000, :mobile_app_id => @mobile_app.id}
    post :place_bid, :mobile_app_bid => {:bid_value => 900, :mobile_app_id => @mobile_app.id}
    flash[:error].should.eql?("Error!!! Please check bid value before you place a bid")
  end
  
  it "placing second bid less to first bid  + $ 50 should be raise error" do
    user = FactoryGirl.build(:user)
    user.confirm!
    sign_in user
    post :place_bid, :mobile_app_bid => {:bid_value => 1000, :mobile_app_id => @mobile_app.id}
    post :place_bid, :mobile_app_bid => {:bid_value => 1040, :mobile_app_id => @mobile_app.id}
    flash[:error].should.eql?("Error!!! Please check bid value before you place a bid")
  end
  
  it "placing second bid equal to first bid  + $ 50 should be raise error" do
    user = FactoryGirl.build(:user)
    user.confirm!
    sign_in user
    post :place_bid, :mobile_app_bid => {:bid_value => 1000, :mobile_app_id => @mobile_app.id}
    post :place_bid, :mobile_app_bid => {:bid_value => 1050, :mobile_app_id => @mobile_app.id}
    flash[:success].should.eql?("Your bid placed successfully!")
  end
  
  it "last bid should be Highest Bid" do
    user = FactoryGirl.build(:user)
    user.confirm!
    sign_in user
    post :place_bid, :mobile_app_bid => {:bid_value => 1001, :mobile_app_id => @mobile_app.id}
    post :place_bid, :mobile_app_bid => {:bid_value => 1051, :mobile_app_id => @mobile_app.id}
    post :place_bid, :mobile_app_bid => {:bid_value => 1251, :mobile_app_id => @mobile_app.id}
    @mobile_app.mobile_app_bid.maximum("bid_value").should.eql?(1251)
  end

end
