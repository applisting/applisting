# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :auction do
    title "MyString"
    description "MyText"
    minimum_price_usd "9.99"
    begins_at "2013-11-22 10:21:38"
    ends_at "2013-11-22 10:21:38"
    sold false
  end
end
