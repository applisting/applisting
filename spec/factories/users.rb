# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do |u|
    u.name "Alindeman"
    u.user_name "alindeman"
    u.email "alindeman@example.net"
    u.password "ilovegrapes"
    u.password_confirmation "ilovegrapes"
  end 
end