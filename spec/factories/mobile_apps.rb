# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mobile_app do |app|
    app.label "android"
    app.app_id "https://play.google.com/store/apps/details?id=com.ea.game.dragonage_na"
  end
end
