# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment do
    amount 1
    auction_id 1
    user_id 1
    credit_uri "MyString"
    debit_uri "MyString"
  end
end
