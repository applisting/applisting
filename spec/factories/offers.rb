# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :offer do
    amount "9.99"
    accepted false
    canceled false
    paid_at "2013-11-22 10:23:20"
  end
end
